#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN1 10
#define SS_PIN2 6
#define RST_PIN 9
MFRC522 mfrc1(SS_PIN1, RST_PIN);
MFRC522 mfrc2(SS_PIN2, RST_PIN);// Create MFRC522 instance.

//******BLUETTOTH******

char inData[1]; // Allocate some space for the string
char loopdata[1]={'9'};
char inChar; // Where to store the character read
byte index = 0; // Index into array; where to store the character

void setup() 
{
  pinMode(10, OUTPUT);
  pinMode(6, OUTPUT);
  Serial.begin(9600);   // Initiate a serial communication
  SPI.begin();// Initiate  SPI bus
  mfrc1.PCD_Init();
  mfrc2.PCD_Init();
  // Initiate MFRC522
  Serial.println("Approximate your card to the reader...");
  Serial.println();
}

void loop() 
{ 
 // if(Serial.available()>0){
  if ( mfrc1.PICC_IsNewCardPresent()) 
  {
   digitalWrite(6,HIGH);//connect ss with 10 which is to be enabled
  
   if (! mfrc1.PICC_ReadCardSerial()) 
  {
    return;
  }
   String content1= "";
  //Show UID on serial monitor
  for (byte i = 0; i < mfrc1.uid.size; i++) 
  {
     content1.concat(String(mfrc1.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content1.concat(String(mfrc1.uid.uidByte[i], HEX));
  }
  if(content1.substring(1) != ""){
  content1.toUpperCase();
  if (content1.substring(1) == "DC F9 DF 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-01");
    Serial.println();
    delay(1000);
  }
   else if(content1.substring(1) == "0D D3 73 5B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-01");
    Serial.println();
    delay(1000);
  }
  else if (content1.substring(1) == "A0 03 DF 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-02");
    Serial.println();
    delay(1000);
  }
  else if (content1.substring(1) == "A9 B2 96 BB") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-02");
    Serial.println();
    delay(1000);
  }
  else if (content1.substring(1) == "0A 2F E3 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-03");
    Serial.println();
    delay(1000);
  }
  else if (content1.substring(1) == "34 ED 96 BB") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-03");
    Serial.println();
    delay(1000);
  }
  else if (content1.substring(1) == "7E B3 73 5B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-04");
    Serial.println();
    delay(1000);
  }
  else {
    Serial.println("NOT RECOGNIZED");
    delay(1000);
  }
  }
    content1= "";
  }
  //*********************
  else if( mfrc2.PICC_IsNewCardPresent())
  {
    digitalWrite(10,HIGH);//connect ss with 6 which is to be enabled
    if ( ! mfrc2.PICC_ReadCardSerial()) 
  {
    return;
  }
  
String content2= "";
for (byte i = 0; i < mfrc2.uid.size; i++) 
  {
     content2.concat(String(mfrc2.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content2.concat(String(mfrc2.uid.uidByte[i], HEX));
  }
  if(content2.substring(1) != ""){

  content2.toUpperCase();
 if (content2.substring(1) == "DC F9 DF 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-01");
    Serial.println();
    delay(1000);
  }
   else if(content2.substring(1) == "0D D3 73 5B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-01");
    Serial.println();
    delay(1000);
  }
  else if (content2.substring(1) == "A0 03 DF 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-02");
    Serial.println();
    delay(1000);
  }
  else if (content2.substring(1) == "A9 B2 96 BB") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-02");
    Serial.println();
    delay(1000);
  }
  else if (content2.substring(1) == "0A 2F E3 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("CARD-03");
    Serial.println();
    delay(1000);
  }
  else if (content2.substring(1) == "34 ED 96 BB") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-03");
    Serial.println();
    delay(1000);
  }
  else if (content2.substring(1) == "7E B3 73 5B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("TAG-04");
    Serial.println();
    delay(1000);
  }
  else {
    Serial.println("NOT RECOGNIZED");
    delay(1000);
  }
   
  }
  content2= "";
  }
  else{
    Serial.write(loopdata,1);
    Serial.println();
    delay(1000);
  }
  //**************BLUETOOTH************
   while (Serial.available() > 0) // Don't read unless
    // there you know there is data
  {
    if (index < 1) // One less than the size of the array
    {
      inChar = Serial.read(); // Read a character
      inData[index] = inChar; // Store it
      index++; // Increment where to write next
      inData[index] = '\0'; // Null terminate the string
    }
  }
  if (index > 0) {
    Serial.write(inData, 1);

  }  
  for (int i = 0; i < 1; i++)
      inData[i] = "";
  index = 0;
//  Serial.write(loopdata,1);
//  Serial.println();
//  delay(2000);

}
//}

  
